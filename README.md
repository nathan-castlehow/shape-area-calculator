# Arc Infrastructure Take Home Coding Challenge

## Challenge Details

The Problem: Develop an application that lets the user select a shape from a list of available options, requests
dimensions for the selected shape, calculates the area of the shape and gives this information back to the user.

Solution requirements: The solution must be written in C# and only use up the available time to develop. It is your
choice what type of application to deliver (mobile, web, desktop, console, other) and how simple or complex you
make it, however we are looking for evidence of quality of design and code over a basic one function application.

## Implementation

The shape calculator was implemented by building a winforms application using c#.
It targets .Net Framework 4.6.1 (but could probably run on alot earlier if tested).
This allows users to select a shape using the combo, enter the relevant dimensions and calculate the area.

### Design Discussion

Original gut reaction was to quickly implement in a web page. This would be simple and not require a backend.
Due to the requirement for extensive C# usage within the application I moved instead to create a winform app.

Shapes were created in their own projects to better support the future structure of dynamically loading the shapes in (see future enhancments for more details).
It perhaps could also be interesting to create a seperate view per shape, as each shape has differing requirements for calculating area.
This was opted against to minimize work, the resulting tradeoff is that the UI is not optimized per shape and some complexity is added with the reflection / databinding as opposed to a straight forward object databind in the winform.

### How to run

- Clone the git repot
- The application can be run by first building the solution with MsBuild / Visual Studio and then either running from the IDE or running the exe from the built application.
Run the tests using MsTest, available in visual studio.

Current Visual Studio Version Visual Studio Community 2017 (15.9.2)

### How to add a new shape

To add a new shape

1) Implement the IShape Interface and reference within the Form Project
	
	The project expects the concrete implementation to make use of 3 Attributes
	- DisplayName -> Class level, used to populate the ComboBox Option Label
	- Description -> Class level, used on the Winform to show a description of the selected shape
	- ShapeDataBind -> Property Level, used to let the winform know it needs to bind to this property.

2) Add to Shape list in MainForm.cs function GetShapeList

The shape will now be available in the combo box for use.

### Testing Strategy

Currently there are basic tests in a single test project. As part of the long term strategy when the Shapes are further seperated out (perhaps into seperate slns) then they should also have a corresponding test project.

### Future Enhancements

For a basic shape area calculator it probably doesn't need to be pushed too far.
Ideally would implement dynamic loading in of shapes based on dropping dlls into a folder to avoid having to rebuild everytime a new shape is added.
It would also be nice to make the layout more flexible + have an icon per selected shape type (a man can dream right?).

## Credits

Wikipedia (Shape Descriptions)
https://en.wikipedia.org/wiki/Square
https://en.wikipedia.org/wiki/Circle

Stackoverflow because life would be incomplete without it.