﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeAreaCalculator.Square;

namespace ShapeAreaCalculator.Test
{
    [TestClass]
    public class ShapeAreaTests
    {
        private const double AreaEpsilon = 0.01;

        [TestMethod]
        public void SquareAreaIs25()
        {
            Square.Square currentSquare = new Square.Square();
            currentSquare.Height = 5;
            currentSquare.Width = 5;

            Assert.AreEqual(currentSquare.Area(), 25);
        }

        [TestMethod]
        public void CircleAreaIs78()
        {
            Circle currentCircle = new Circle();
            currentCircle.Radius = 5;

            // result is not a rational number, check within allowable epsilon
            Assert.IsTrue(Math.Abs(currentCircle.Area() - 78.54) <= AreaEpsilon);
        }
    }
}
