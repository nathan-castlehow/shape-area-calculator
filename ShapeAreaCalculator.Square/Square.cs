﻿using ShapeAreaCalculator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeAreaCalculator.Square
{
    [DisplayName("Square")]
    [Description("A square is a regular quadrilateral, which means that it has four equal sides and four equal angles. Area calculation width * height.")]
    public class Square : IShape
    {
        [ShapeDataBind]
        public double Width { get; set;}

        [ShapeDataBind]
        public double Height { get; set;}       

        public Square()
        {
            this.Width = 0;
            this.Height = 0;
        }

        public Square(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }
        public double Area()
        {
            return Width * Height;
        }
    }
}
