﻿using ShapeAreaCalculator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeAreaCalculator.Square
{
    [DisplayName("Circle")]
    [Description("A circle is a shape consisting of all points in a plane that are a given distance from a given point, the centre. Area calculation PI * r * r.")]
    public class Circle : IShape
    {
        [ShapeDataBind]
        public double Radius { get; set; }


        public Circle()
        {
            this.Radius = 0;
        }

        public Circle(double radius)
        {
            this.Radius = radius;
        }

        public double Area()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
