﻿using ShapeAreaCalculator.Model;
using ShapeAreaCalculator.Square;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeAreaCalculator
{
    public partial class Main : Form
    {
        private IShape currentShape = null;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

            ShapeOptionCombo.DataSource = GetShapeList();

        }

        private List<IShape> GetShapeList()
        {
            // todo add dynamic registration of shapes (search folder for dlls or similar)
            // this will remove the need for explicit dependencies on new shapes
            List<IShape> currentShapes = new List<IShape>();
            currentShapes.Add(new Square.Square());
            currentShapes.Add(new Circle());

            // ADD NEW SHAPES HERE

            return currentShapes;
        }

        // Set the ComboOptionText to use the DisplayName attribute off the concrete class
        private void ShapeOptionCombo_Format(object sender, ListControlConvertEventArgs e)
        {
                IShape currentShape = (IShape)e.ListItem;
                e.Value = Utility.GetDisplayName(currentShape.GetType());
        }

        private void ShapeOptionCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            currentShape = (ShapeOptionCombo.SelectedValue as IShape);

            // Set description of currently selected shape
            SelectedShapeDescription.Text = Utility.GetDescription(currentShape.GetType());

            //reset result field
            SelectedShapeAreaResult.Text = "-";

            // update controls / bindings to match current shape type
            var properties = currentShape
                                        .GetType()
                                        .GetProperties()
                                        .Where(x => Attribute.IsDefined(x, typeof(ShapeDataBind)))
                                        .Select(x => new ShapeDataBindContainer { Field = x.Name, Value = 0.00})
                                        .ToList();


            SelectedShapeFields_Grid.DataSource = properties;
            SelectedShapeFields_Grid.Update();

        }

        private void SelectedShape_CalculateArea_Click(object sender, EventArgs e)
        {
            // update shape fields with user entered data
            foreach(ShapeDataBindContainer field in (SelectedShapeFields_Grid.DataSource as List<ShapeDataBindContainer>))
            {
                Type type = currentShape.GetType();

                PropertyInfo prop = type.GetProperty(field.Field);

                prop.SetValue(currentShape, field.Value, null);
            }
            
            // Set current calculated area result
            SelectedShapeAreaResult.Text = currentShape.Area().ToString("0.00");
        }

    }
}
