﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeAreaCalculator
{
    public static class Utility
    {
        
        private static Attribute GetAttributeValue(Type objectType,Type attribute)
        {
            System.Attribute attr = System.Attribute.GetCustomAttribute(objectType,attribute);
            return attr;
        }

        public static string GetDisplayName(Type objectType)
        {
            string currentValue = "(unknown)";
            var currentAttr = GetAttributeValue(objectType,typeof(DisplayNameAttribute));

            if(currentAttr is DisplayNameAttribute)
            {
               currentValue = (currentAttr as DisplayNameAttribute).DisplayName;
            }

            return currentValue;
        }

        public static string GetDescription(Type objectType)
        {
            string currentValue = "(unknown)";
            var currentAttr = GetAttributeValue(objectType, typeof(DescriptionAttribute));

            if (currentAttr is DescriptionAttribute)
            {
                currentValue = (currentAttr as DescriptionAttribute).Description;
            }

            return currentValue;
        }
    }
}
